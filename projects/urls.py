from django.urls import path
from projects.views import list_projects, show_task, create_project

urlpatterns = [
    path("projects/", list_projects, name="list_projects"),
    path("projects/<int:id>/", show_task, name="show_project"),
    path("projects/create/", create_project, name="create_project"),
]
