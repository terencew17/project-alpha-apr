from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": project,
    }
    return render(request, "projects/list_project.html", context)


@login_required
def show_task(request, id):
    tasks = Task.objects.filter(project=id)
    projects = Project.objects.filter(owner=request.user, id=id)
    context = {
        "show_task": tasks,
        "list_projects": projects,
    }
    return render(request, "projects/show_task.html", context)


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")

    else:
        form = ProjectForm
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
